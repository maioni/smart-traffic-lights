class Sensor < ActiveRecord::Base
	belongs_to :street_section
	has_many :actuators, through: :actuator_sensors
end
