/*=======================================
 *
 * TrafficSlider class
 *
 *---------------------------------------
 * This object represents the slider
 * controller available on the interface, 
 * responsible for filtering traffic flow
 * data according to specific time frames
 ========================================*/

//var TrafficSlider = function(initState) {
//    this.slider = $('#slider').slider();
//    this.currentState = initState;
//}
//
//TrafficSlider.prototype = {
//
//    constructor: TrafficSlider,
//
//    getCurrentState: function() {
//        this.currentState = this.slider.slider('getValue');
//        return this.currentState;
//    },
//    
//    setCurrentState: function(state) {
//        this.slider.slider('setValue', state);
//        this.currentState = state;
//    }
//}