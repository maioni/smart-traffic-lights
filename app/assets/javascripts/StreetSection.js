/*=======================================
 *
 * StreetSection class
 *
 *---------------------------------------
 * This is an object responsible for
 * representing a street section that is 
 * monitored by a single sensor, i.e.,
 * there is a one-to-one relationship
 * between section and sensor. Furthermore,
 * a section must store state information
 * so that can be displayed appropriately,
 * like colorings for traffic speed and
 * volume, for example.
 ========================================*/

var StreetSection = function(location, speedColor, volumeColor, map, id) {
    this.section = new google.maps.Polyline({
        path: location,
        geodesic: true,
        strokeColor: speedColor,
        strokeOpacity: .8,
        strokeWeight: 5,
        zIndex: 9,
        map: map.map
    });
    this.speedColor = speedColor;
    this.volumeColor = volumeColor;
    this.map = map;
    this.trafficSpeed = 0;
    this.vehicles = 0;
    this.mode = "speed";
    this.infoWindow = new google.maps.InfoWindow();
    this.section.addListener('click', this.setInfoWindow());
    this.sensorId = id;

    this.fiveMinutesSpeed = [];
    this.fiveMinutesFlux  = [];
    this.oneHourSpeed = [];
    this.oneHourFlux  = [];
    this.aDaySpeed = [];
    this.aDayFlux  = [];
    this.slider = $('#slider').slider();

    var section = this;
    window.setInterval(function(){ section.updateColor(section); }, 2000);
}

StreetSection.prototype = {

    constructor: StreetSection,

    setSpeedColor: function(color) {
        this.speedColor = color;
        if (this.mode == 'speed')
            this.section.setOptions({strokeColor: color});
    },

    setVolumeColor: function(color) {
        this.volumeColor = color;
        if (this.mode == 'volume')
            this.section.setOptions({strokeColor: color});
    },

    changeToSpeedMode: function() {
        this.mode = 'speed';
        this.section.setOptions({strokeColor: this.speedColor});
    },

    changeToVolumeMode: function() {
        this.mode = 'volume';
        this.section.setOptions({strokeColor: this.volumeColor});
    },

    changeWidth: function(width) {
        this.section.setOptions({strokeWeight: width});
    },

    setVisibility: function(boolean) {
        this.section.setOptions({visible: boolean});
    },

    setInfoWindow: function() {
        var section = this;
        return function(e) {
            var contentString = '<div class="InfoWindowText">' +
                '<p> Traffic Speed: ' + section.trafficSpeed + '</p>' +
                '<p> Number of vehicles: '+ section.vehicles + '</p>' +
                //'<object type="application/x-vlc-plugin" pluginspage="http://www.videolan.org" data="https://test01-alextsc.c9users.io/assets/traffic.avi" width="400" height="300" id="video1">' +
                //'<param name="movie" value="https://test01-alextsc.c9users.io/assets/traffic.avi"/>' +
                //'<embed type="application/x-vlc-plugin" name="video1"' +
                //'autoplay="yes" loop="no" width="400" height="300"' +
                //'target="https://test01-alextsc.c9users.io/assets/traffic.avi" />' +
                //'</object>' +

                //'<iframe src="file:///assets/traffic.avi" width="150px" height="100px" frameborder="0" allowfullscreen></iframe>' +
                '<iframe src="https://camerite.com/embed/241/sao-paulo/sao-paulo/rua-da-consolacao-n-2418?autoplay=true&sound=false" width="150px" height="100px" frameborder="0" allowfullscreen></iframe>' +
                '</div>';
            section.map.closeInfoWindows();
            section.infoWindow.setOptions({
                content: contentString,
                position: e.latLng
            });
            section.infoWindow.open(section.map.map);
        }
    },

    closeInfoWindow: function() {
        this.infoWindow.close();
        return true;
    },

    hashSpeedColor: function(numeric) {
        if (numeric < 10.0) return '#008000';
        else if (numeric >= 10.0 && numeric < 20.0) return '#abc837';
        else if (numeric >= 20.0 && numeric < 30.0) return '#d4aa00';
        else if (numeric >= 30.0 && numeric < 40.0) return '#ff6600';
        else if (numeric >= 40.0 && numeric < 50.0) return '#d40000';
        else return '#2b1100';
    },

    hashVolumeColor: function(numeric) {
        if (numeric < 1.66) return '#ffd5d5';
        else if (numeric >= 1.66 && numeric < 3.33) return '#de87aa';
        else if (numeric >= 3.33 && numeric < 5.0)  return '#ff2a7f';
        else if (numeric >= 5.0  && numeric < 6.66) return '#aa0088';
        else if (numeric >= 6.66 && numeric < 8.33) return '#441650';
        else return '#241c1f';
    },

    updateTimeFrame: function(array, size, value) {
        array.push(value);
        if (array.length > size) array.shift();
        var sum = 0;
        for (i in array)
            sum += array[i];
        return sum / array.length;
    },

    setColors: function(value, speedList, densityList) {
        switch(value) {
            case 1: return {speed: speedList[0], flux: densityList[0]}; //now
            case 2: return {speed: speedList[1], flux: densityList[1]}; //5min
            case 3: return {speed: speedList[2], flux: densityList[2]}; //1hour
            case 4: return {speed: speedList[3], flux: densityList[3]}; //1day
        }
    },

    updateColor: function(obj) {
        return $.get("sensor/" + obj.sensorId).then(function(data) {
            obj.trafficSpeed = data.speed;
            obj.vehicles = data.flux;
            var s1 = data.speed;
            var d1 = data.flux;
            var s2 = obj.updateTimeFrame(obj.fiveMinutesSpeed, 75, data.speed);
            var d2 = obj.updateTimeFrame(obj.fiveMinutesFlux, 75, data.flux); 
            var s3 = obj.updateTimeFrame(obj.oneHourSpeed, 450, data.speed);
            var d3 = obj.updateTimeFrame(obj.oneHourFlux, 450, data.flux);
            var s4 = obj.updateTimeFrame(obj.aDaySpeed, 10800, data.speed);
            var d4 = obj.updateTimeFrame(obj.aDayFlux, 10800, data.flux);
            var value = obj.slider.slider('getValue');
            var color = obj.setColors(value, [s1, s2, s3, s4], [d1, d2, d3, d4]);
            obj.setSpeedColor(obj.hashSpeedColor(color.speed));
            obj.setVolumeColor(obj.hashVolumeColor(color.flux));
            return true;
        });
    }
}