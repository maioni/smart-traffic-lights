class TrafficLightController < ApplicationController
    def list
        @traffic_lights = TrafficLight.all
        render :json => @traffic_lights
    end

    def show 
        @traffic_light = TrafficLight.find(params[:id])
        render :json => @traffic_light
    end
    
    def edit
        @traffic_light = TrafficLight.where(id: params[:id])
        for i in @traffic_light
            if i.init_state == false
                i.init_state = true
            else
                i.init_state = false
            end
            i.save
        end
        render :json =>@traffic_light
    end

end
