Rails.application.routes.draw do

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  #resources maps
  # You can have the root of your site routed with "root"
  get 'stl/index'

  root 'stl#index'
  

  get 'street_section' => 'street_section#list'
  
  get 'actuator_sensor' => 'actuator_sensor#list'
  
  get 'actuator/:id' => 'actuator#show'

  get 'street_section/:id' => 'street_section#show'

  get 'sensor/:id' => 'sensor#show'
  
  get 'sensor' => 'sensor#list'

  get 'traffic_light' => 'traffic_light#list'

  get 'traffic_light/:id' => 'traffic_light#show'
  
  get 'traffic_light/edit/:id' => 'traffic_light#edit'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
