require 'test_helper'

class ActuatorSensorTest < ActiveSupport::TestCase
  test "it should be possible to have more than one actuator for the same sensor" do
  	act1 = ActuatorSensor.find(1).actuator_id
  	act2 = ActuatorSensor.find(3).actuator_id
  	sns1 = ActuatorSensor.find(1).sensor_id
  	sns2 = ActuatorSensor.find(3).sensor_id
    assert_not_equal act1, act2
    assert_equal sns1, sns2
  end

  test "it should be possible to have more than one sensor for the same actuator" do
    act1 = ActuatorSensor.find(1).actuator_id
  	act2 = ActuatorSensor.find(2).actuator_id
  	sns1 = ActuatorSensor.find(1).sensor_id
  	sns2 = ActuatorSensor.find(2).sensor_id
    assert_not_equal sns1, sns2
    assert_equal act1, act2
  end
end
