require 'test_helper'

class ActuatorTest < ActiveSupport::TestCase
  test "two actuators never belong to the same crossing" do
  	loc1 = [Actuator.find(1).latitude, Actuator.find(1).longitude]
  	loc2 = [Actuator.find(2).latitude, Actuator.find(2).longitude]
    assert_not_equal loc1, loc2
  end

  test "latitude is a float" do
    assert_kind_of 1.0.class, Actuator.find(1).latitude
  end

  test "longitude is a float" do
    assert_kind_of 1.0.class, Actuator.find(1).longitude
  end

  test "resource is a string" do
  	assert_kind_of "address".class, Actuator.find(1).resource
  end
end
