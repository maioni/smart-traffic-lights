require 'test_helper'

class TrafficLightTest < ActiveSupport::TestCase
  
  test "traffic lights belongs to the same actuator" do
    assert_equal 1, TrafficLight.find(1).actuator_id
    assert_equal 1, TrafficLight.find(2).actuator_id
  end

  test "latitude is a float" do
    assert_kind_of 1.0.class, TrafficLight.find(1).latitude
  end

  test "longitude is a float" do
    assert_kind_of 1.0.class, TrafficLight.find(1).longitude
  end

  test "image rotation is an integer" do
    assert_kind_of 1.class, TrafficLight.find(1).img_rotation
  end

  test "initial state is boolean" do
  	assert_kind_of false.class, TrafficLight.find(1).init_state
  end

  test "initial img is a string" do
  	assert_kind_of "addres".class, TrafficLight.find(1).init_img
  end
end
